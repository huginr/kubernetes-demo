# Getting Started

### Build container image and push to registry

1. Update the container tag in pom.xml
2. Build the image with `mvn clean install`
3. Push the image to registry `docker push registry.gitlab.com/huginr/kubernetes-demo:v2`

### Run with docker

`docker run -p 8080:8080 registry.gitlab.com/huginr/kubernetes-demo:v1`

### Find the exposed ip and port of a NodePort service

`minikube service --url kubernetes-demo-service`

-> `http://192.168.49.2:30007`


