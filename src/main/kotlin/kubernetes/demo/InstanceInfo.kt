package kubernetes.demo

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter.ISO_DATE_TIME

data class InstanceInfo(
    val name: String,
    val version: String,
    val date: String = LocalDateTime.now().format(ISO_DATE_TIME)
)