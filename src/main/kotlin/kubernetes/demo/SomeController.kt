package kubernetes.demo

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import net.datafaker.Faker
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.util.Locale

@RestController
@RequestMapping("/")
class SomeController {
    private val instanceName = Faker(Locale("nl")).funnyName().name()
    private val applicationVersion = "Version 2.0 (Panda Chicken) \uD83D\uDC3C\uD83D\uDC14"

    private var aliveAndKicking = true

    @GetMapping("/instance-info")
    fun instanceInfo() =
        if (aliveAndKicking) {
            ResponseEntity.ok()
                .header("Connection", "close")
                .body(
                    InstanceInfo(
                        instanceName,
                        applicationVersion
                    )
                )
        } else {
            ResponseEntity
                .internalServerError()
                .header("Connection", "close")
                .body("..uh")
        }

    @GetMapping("/self-destruct")
    fun selfDestruct() =
        ResponseEntity.ok()
            .header("Connection", "close")
            .body("Bye world")
            .also { doSelfDestruct() }

    private fun doSelfDestruct() {
        CoroutineScope(Dispatchers.Default).launch {
            println("Self destruct initiated. Slowly dying...")
            delay(500)
            aliveAndKicking = false
        }
    }
}
