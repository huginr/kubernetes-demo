package kubernetes.demo

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class DemoApplication

fun main(args: Array<String>) {
	println("Slow down brother..")
	Thread.sleep(10000)
	runApplication<DemoApplication>(*args)
}
